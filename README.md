# Tswelopele Frail Care Centre [![Netlify Status](https://api.netlify.com/api/v1/badges/1ec802a2-d286-4f7a-8a34-2fdf0502ef25/deploy-status)](https://app.netlify.com/sites/tfcc/deploys)

Static Website for Tswelopele Frail Care Centre.

Dev Dependencies:

```
Webpack v4.29.6
Bootstrap v4.5.0
Dart Sass v1.32.4
```

#### How do I get set up?

- npm install
- npm start

#### Who do I talk to?

- @bitbucket/mrq\_ or @github/mohau-r

#### TaskList

Beneficiary Page

- [x] Remove the following as they are either deceased or discharged.
    - Mr Andre Olivier
    - Mr Willem Nel
    - Mr Nelson Kgobe
    - Ms Jessie van Aswegen
    - Ms Gladys Mkhare
    - Mr David Mthembu
    - Mr Elijah Malinga

ContactUs Page

- [x] Typographical Errors
    - 27/7 to 24/7

AboutUs Page

- [] Board Member(s)
    - [] Ms M Kubeka
    - [] Mr M Shandukane
    - [x] Mrs A Phoshoko
    - [x] Mzwa Ndimande
        - Treasurer
    - [x] Jabulani Dubula
        - Chairperson
    - [x] Letlhogonolo Gaborone
        - Board Member

