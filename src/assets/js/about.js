const about = require("./../scss/about.scss")
const teamImg = require.context("../img/t/p", true, /\.(png|jpe?g)$/)
import { each } from "lodash-es"
import { founders, advisoryBoard, staff } from "./data/aboutJson"
// console.info("About...")

$(document).ready(function () {
	let files = {}
	teamImg.keys().forEach((filename) => {
		files[filename] = teamImg(filename)
	})

	let count = 0,
		item_per_row = 3,
		$listFounders = $("#listFounders"),
		$list = ``,
		$listAdvisoryBoard = $("#listAdvisoryBoard"),
		$list2 = ``,
		$list3 = ``

	let $listStaff = $("#listStaff")
	let listHtml = function (data) {
		let $disp = ``
		let $footer = ``
		let fullName = data.firstName + " " + data.lastName
		let imgURL = data.imgsrc
		let c = (
			data.firstName.charAt(0) +
			"" +
			data.lastName.charAt(0)
		).toLowerCase()

		if (jQuery.isEmptyObject(data.social)) {
			// console.log("Javscript Object is empty")
			// $footer += `<div class="card-footer" hidden></div`
			// return $footer
		} else {
			each(data.social, (d) => {
				let tw = d.tw,
					lkdn = d.lkdn,
					readme = d.readme,
					socialCl = tw
						? ""
						: "hidden" && lkdn
						? ""
						: "hidden" && readme
						? ""
						: "hidden"

				$footer += `
				<div ${socialCl}  class="card-footer">
					<div ${socialCl} class="social-bar">
						<div class="social-buttons">
							<a ${tw ? "" : "hidden"} class="tw-ic" target="_blank">
								<i class="fa fa-twitter white-text mr-4"></i>
							</a>
							<a ${lkdn ? "" : "hidden"} class="li-ic" target="_blank" href="${lkdn}">
								<i class="fa fa-linkedin white-text mr-4"> </i>
							</a>
							<a ${readme ? "" : "hidden"} class="gplus-ic" target="_blank" href="${readme}">
								<i class="fa fa-readme white-text"> </i>
							</a>
						</div>
					</div>		
				</div>
				`
				return $footer
			})
		}

		if (!imgURL || imgURL === null || imgURL === undefined) {
			imgURL = "/assets/img/default.png"
		}

		if (count === 0) {
			// Start of a row
			$disp += `
				<div class="row">
				`
		}
		$disp += `
			<div class="col-sm-6 col-lg-4">
				<div class="card">
					<div class="card-img-top ${c}" style="background-image: url(${imgURL})"></div>
					<div class="card-header">${fullName}</div>
					<div class="card-body">
						<h6 class="card-pos text-muted">${data.position}</h6>
						<p class="card-text ${c}">${data.description}</p>
					</div>
					${$footer}
				</div>
			</div>
		`

		++count
		if (count === item_per_row) {
			// End of row
			$disp += `
				</div>
				`
			count = 0
		}
		return $disp
	}

	/*
	 * founding members
	 */
	each(founders, function (data) {
		$list += listHtml(data)
	})

	$listFounders.html($list)

	/*
	 * Staff members
	 */

	each(staff, function (data) {
		$list3 += listHtml(data)
	})

	$listStaff.html($list3)

	/*
	 * Board members
	 */

	let listBoard = function (data) {
		let $disp = ``,
			$footer = ``,
			fullName = data.firstName + " " + data.lastName,
			imgURL = data.imgsrc,
			c = (
				data.firstName.charAt(0) +
				"" +
				data.lastName.charAt(0)
			).toLowerCase()

		if (jQuery.isEmptyObject(data.social)) {
			// console.log("Javscript Object is empty")
			// $footer += `<div class="card-footer" hidden></div`
			// return $footer
		} else {
			each(data.social, (d) => {
				let tw = d.tw,
					lkdn = d.lkdn,
					readme = d.readme,
					socialCl = tw
						? ""
						: "hidden" && lkdn
						? ""
						: "hidden" && readme
						? ""
						: "hidden"

				$footer += `
				<div ${socialCl} class="card-footer">
					<div ${socialCl} class="social-bar">
						<div class="social-buttons">
							<a ${tw ? "" : "hidden"} class="tw-ic" target="_blank">
								<i class="fa fa-twitter white-text mr-4"></i>
							</a>
							<a ${lkdn ? "" : "hidden"} class="li-ic" target="_blank" href="${lkdn}">
								<i class="fa fa-linkedin white-text mr-4"> </i>
							</a>
							<a ${readme ? "" : "hidden"} class="gplus-ic" target="_blank" href="${readme}">
								<i class="fa fa-readme white-text"> </i>
							</a>
						</div>
					</div>		
				</div>
				`
				return $footer
			})
		}

		if (!imgURL || imgURL === null || imgURL === undefined) {
			imgURL = "/assets/img/default.png"
		}

		if (count === 0) {
			/*optional stuff to do after success */
			// Start of a row
			$disp += `
				<div class="row">
				<div class="card-deck mx-auto">
				`
		}

		$disp += `
			<div class="col-sm-6 col-lg-4">
				<div class="card">
					<div class="card-img-top ${c}" style="background-image: url(${imgURL})"></div>
					<div class="card-header">${fullName}</div>
					<div class="card-body">
						<h6 class="card-pos text-muted">${data.position}</h6>
						<p class="card-text ${c}">${data.description}</p>
					</div>
					${$footer}
				</div>
			</div>
			`

		++count
		if (count === item_per_row) {
			// End of row
			$disp += `
				</div>
				</div>
				`
			count = 0
		}
		return $disp
	}
	each(advisoryBoard, function (data) {
		$list2 += listBoard(data)
	})

	$listAdvisoryBoard.html($list2)
})
