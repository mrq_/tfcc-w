const beneficiaries = require("../scss/beneficiaries.scss")
import { filter, each } from "lodash-es"
import beneficiariesJson from "./data/beneficiariesJson"

const beneficiaryImg = require.context("../img/b/", true, /\.(png|jpe?g)$/)

// console.info("Beneficiaries...")
$(document).ready(function() {
	let files = {}

	beneficiaryImg.keys().forEach(filename => {
		files[filename] = beneficiaryImg(filename)
	})

	let count = 0,
		item_per_row = 2,
		$listBeneficiaries = $("#listBeneficiaries"),
		$list = ``,
		$dischargedBeneficiaries = $("#dischargedBeneficiaries"),
		$list_2 = ``,
		listHtml = function(data) {
			let $disp = ``,
				fullName = data.firstName + " " + data.lastName,
				doa = data.doa,
				descrip = data.description,
				publication = data.publication,
				imgURL = data.imgsrc

			if (!imgURL || imgURL === null || imgURL === undefined) {
				imgURL = "/assets/img/b/default.png"
			}

			if (count === 0) {
				/*optional stuff to do after success */
				// Start of a row
				$disp += `
				<div class="row mb-3">
				`
			}

			$disp += `
			<div class="col-lg-6">
				<ul class="list-unstyled">
					<li class="media">
						<div class="img mr-3" style="background-image: url(${imgURL})"></div>
						<div class="media-body">
							<h5 class="media-heading mb-0">${fullName}</h5>
							<p class="media-category mb-1"><small class="text-muted">${doa}</small></p>
							<p class="media-description mt-2 mb-1">${descrip}</p>
							<p class="media-publication mt-2"><small>${publication}</small></p>
						</div>
					</li>
				</ul>
			</div>
			`

			++count
			if (count === item_per_row) {
				// End of row
				$disp += `
				</div>
				`
				count = 0
			}
			return $disp
		}

	each(beneficiariesJson, function(data) {
		if (
			data.discharged === false &&
			data.deceased !== true &&
			!data.discharged === true
		) {
			$list += listHtml(data)
		} else {
			if (!data.discharged === false && data.discharged === true) {
				$list_2 += listHtml(data)
			}
		}
	})

	/**
	 * List Beneficiaries
	 */
	if (count > 0) {
		// Close the last row if it exist.
		$list += `
			</div>
			`
	}
	$listBeneficiaries.html($list)

	/**
	 * Discharged beneficiaries list
	 */
	if (count > 0) {
		// Close the last row if it exist.
		$list_2 += `
			</div>
			`
	}
	$dischargedBeneficiaries.html($list_2)
})
