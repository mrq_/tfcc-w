// console.info("Services...")
const service = require("./../scss/service.scss")

import fd from "./data/loadFD"
const fdImages = require.context("../img/fd/", true, /\.(png|jpe?g)$/)

$(document).ready(function () {
	// console.info("jQuery Ready!")

	/**
	 * Import all Funders & Donors Images in image folder
	 **/
	// console.info("Import all Funders & Donors Images")
	let files = {},
		$target = $("#listFD"),
		$html = ``,
		count = 0,
		item_per_row = 3

	fdImages.keys().forEach((filename) => {
		files[filename] = fdImages(filename)
	})

	$.each(fd, function (index, data) {
		let name = data.name
		let descrip = data.description
		let imgURL = data.imgsrc

		if (!imgURL || imgURL === null || imgURL === undefined) {
			imgURL = "./assets/img/default.png"
		}

		let backgroundImg = `style="background-image: url(${imgURL})"`

		/*optional stuff to do after success */
		if (count === 0) {
			// Start of a row
			$html += `
				<div class="card-group">
				`
		}

		$html += `
		<div class="card">
			<img class="card-img-top ${name}" src="${imgURL}" alt="${name}" />
			<div class="card-body">
				<h5 class="card-title">${name}</h5>
				<p class="card-text">${descrip}</p>
			</div>
		</div>
		`
		++count
		if (count === item_per_row) {
			// End of row
			$html += `
				</div>
				`
			count = 0
		}

		//I only put this to show what these variables do for you
		// console.log("At index " + index + ", there is a value of " + fullName)
	})

	if (count > 0) {
		// Close the last row if it exist.
		$html += `
			</div>
			`
	}
	$target.html($html)
})
