const path = require("path")
const gallery = require("./../scss/gallery.scss")

const reqImages = require.context("../img/g/", true, /\.(png|jpe?g)$/)
import "simplelightbox"

// console.info("Gallery...")

$(document).ready(function() {
	// console.info("jQuery Ready!")

	/**
	 * Import all Gallery Images in image folder
	 **/
	// console.info("Import all Gallery Images")
	const obj = {}
	reqImages.keys().forEach(key => {
		let allImagesFilepaths = key.split("./").pop() // remove the first 2 characters
		let album = allImagesFilepaths.toUpperCase().split("/")[0]

		obj[allImagesFilepaths] = reqImages(key)
		// console.log(reqImages(key))

		let $showcaseList = $(".showcase-list")
		let $someAnchor = $("<a/>", {
			href: reqImages(key),
			class: "showcase"
		}).appendTo($showcaseList)
		let $imgSrc = $("<div />", {
			class: "image wallpaper",
			style: "background-image:url(" + reqImages(key) + ");",
			title: album
		}).appendTo($someAnchor)
	})

	let $gallery = $(".gallery a").simpleLightbox({
		overlay: true,
		spinner: true,
		loop: false,
		disableScroll: true,
		disableRightClick: true,
		scaleImageToRatio: true,
		docClose: false,
		captionSelector: "div",
		doubleTapZoom: 2,
		alertError: false
	})

	$("button.launch").click("open.simplelightbox", function() {
		$gallery.open()
	})
	$gallery
		.on("show.simplelightbox", function() {
			// console.log("Requested for showing")
		})
		.on("close.simplelightbox", function(e) {
			e.preventDefault()
			// console.log("Requested for closing")
		})
		.on("error.simplelightbox", function(e) {
			e.preventDefault()
			console.log(e) // some usefull information
		})
})
