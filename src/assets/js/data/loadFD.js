const fd = [
	{
		name: "Hot Slots",
		description: "",
		imgsrc: "./assets/img/fd/hotSlots.png",
	},
	{
		name: "National Lotteries Commission",
		description: "",
		imgsrc: "./assets/img/fd/nlc.png",
	},
	{
		name: "National Lotteries Commission",
		description: "",
		imgsrc: "./assets/img/fd/nlppp.png",
	},
	{
		name: "National Lottery Distribution Trust Fund",
		description: "",
		imgsrc: "./assets/img/fd/nldtf.png",
	},
	{
		name: "Gauteng Province of Social Development",
		description: "",
		imgsrc: "./assets/img/fd/gsd.jpg",
	},
	{
		name: "Department of Social Development",
		description: "",
		imgsrc: "./assets/img/fd/dsd.jpg",
	},
	{
		name: "Department of Health",
		description: "",
		imgsrc: "./assets/img/fd/health.jpg",
	},
	{
		name: "City of Joburg",
		description: "",
		imgsrc: "./assets/img/fd/cityofjoburg.png",
	},
]

export default fd
