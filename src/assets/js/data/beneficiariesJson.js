const beneficiaries = [
	{
		firstName: "Ntemi Tuis",
		lastName: "Mqwathi",
		doa: "Date Of Admission - 2017/02/04",
		description:
			"He was admitted from Tambo Memorial hospital in Boksburg.He cannot recall the address of the next of kin or family.Difficult to trace his family.",
		publication: "",
		imgsrc: "/assets/img/b/ntemi_mqwathi.png",
		deceased: null,
		discharged: false
	},
	{
		firstName: "Henry",
		lastName: "Hammer",
		doa: "Date Of Admission - 2017/02/16",
		description:
			"Admitted from the Johannesburg streets. He alleges that he was staying at 2 Piet My Vrou Street, Brits which was a ministry but the people were abusing drugs. He decided to come to Johannesburg. He alleges that all his family have passed on",
		publication: "",
		imgsrc: "/assets/img/b/henry_hammer.png",
		deceased: true,
		discharged: false
	},
	
	{
		firstName: "Johannes",
		lastName: "Strauss",
		doa: "Date Of Admission - 2018/06/18",
		description:
			"Admitted from a flat in Yeoville where he was staying alone and was unable to look after himself. His sister Esmelda is staying somewhere in Pretoria address is unknown. He left his wife and children in Cape Town.",
		publication: "",
		imgsrc: "/assets/img/b/johannes_strauss.png",
		deceased: null,
		discharged: false
	},
	{
		firstName: "Dumisani",
		lastName: "Khumalo",
		doa: "Date Of Admission - 1996/01/19",
		description:
			"Admitted from Zanele Mbeki Home in Dunnator, Nigel. He alleges that he has family in Alexandra.",
		publication: "",
		imgsrc: "/assets/img/b/dumisani_khumalo.png",
		deceased: null,
		discharged: false
	},
	
	{
		firstName: "Charles",
		lastName: "Botha",
		doa: "Date Of Admission - 1996/01/19",
		description:
			"Admitted from Boksburg Benoni Hospital now called Tambo Memorial Hospital. He was from 116 Kokozela Street, Bochabela in Bloemfontein.",
		publication: "",
		imgsrc: "/assets/img/b/charles_botha.png",
		deceased: null,
		discharged: false
	},
	{
		firstName: "Alex",
		lastName: "Msimanga",
		doa: "Date Of Admission - 2013/06/14",
		description:
			"He was admitted from Pretoria Hospital. No name, no document. Name and surname was given by Social Worker, Applied for ID book and grant.",
		publication: "Published twice in Daily Sun Newspaper, with no response.",
		imgsrc: "/assets/img/b/alex_msimanga.png",
		deceased: null,
		discharged: false
	},
	{
		firstName: "Themba",
		lastName: "Dladla",
		doa: "Date Of Admission - 2014/02/03",
		description:
			"Admitted from Zanele Mbeki in Nigel. His nephew Robert was traced in 2013 and promised to look for his family in Greytown, Emabomvini. He disappeared and never came back. The contact number he gave no longer works.",
		publication: "",
		imgsrc: "/assets/img/b/themba_dladla.png",
		deceased: null,
		discharged: false
	},
	{
		firstName: "Bertina Plazini",
		lastName: "Buthelezi",
		doa: "Date Of Admission - 1999/07/07",
		description:
			"Originally from Mlambokazi, Kwa Mpunda under Chief Felafuthi Mdlalose. She came to Johannesburg long time ago to work as a domestic worker.",
		publication: "Published twice in Daily Sun Newspaper, with no response.",
		imgsrc: "/assets/img/b/bertina_buthelezi.png",
		deceased: null,
		discharged: false
	},
	{
		firstName: "Booi",
		lastName: "Phundulu",

		doa: "Date Of Admission - 2001/11/12",
		description:
			"Admitted from Pretoria Hospital, had head injury. He cannot remember where he comes from.",
		publication: "Published twice in Daily Sun Newspaper, with no response.",
		imgsrc: "/assets/img/b/booi_phundulu.png",
		deceased: null,
		discharged: false
	},

	{
		firstName: "Isaac",
		lastName: "Shangai",
		doa: "Date Of Admission - 2008/09/1",
		description:
			"Admitted from Edenvale. Originates from 112 Kgapamadi Street, Stinkwater in Hammanskraal. His brother is Vuma. He lost contact with the brother.",
		publication: "Published twice in Daily Sun Newspaper, with no response.",
		imgsrc: "/assets/img/b/isaac_shangai.png",
		deceased: null,
		discharged: false
	},
	
	{
		firstName: "Flora",
		lastName: "Nkoana",
		doa: "Date Of Admission - 2012/01/11",
		description:
			"Her real name is Zinukile Mvelase originally from Kwa-Phongote under chief Mkhonza Mvelase. Her sister’s name is Ntombendiya Kunene, brother is Mandlenkosi Mvelase. Her landmark is Malinga beer hall.",
		publication: "Published twice in Daily Sun Newspaper, with no response.",
		imgsrc: "/assets/img/b/flora_nkoana.png",
		deceased: null,
		discharged: false
	},
	{
		firstName: "James 'Malgas'",
		lastName: "Manong",
		doa: "Date Of Admission - 2012/02/11",
		description:
			"Is originally from Roodepoort. He alleges that his family is at 4008 Molotswa Street in Dobsonville. His other origins are Kimberly and Port Elizabeth",
		publication: "",
		imgsrc: "/assets/img/b/james_manong.png",
		deceased: null,
		discharged: false
	},
	{
		firstName: "Prince",
		lastName: "Izak",
		doa: "Date Of Admission - 2012/07/11",
		description:
			"He was admitted from Tambo Memorial Hospital. He started that he is from De Aar and came to Johannesburg at a very young age +- 16 years to look for a job. He lost contact with his family and cannot even remember the address.",
		publication: "Published twice in Daily Sun Newspaper, with no response.",
		imgsrc: "/assets/img/b/prince_izak.png",
		deceased: null,
		discharged: false
	},
	{
		firstName: "Petrus",
		lastName: "Fourie",
		doa: "Date Of Admission - 2014/04/02",
		description:
			"Admitted from Bertha Gxowa hospital in Germiston. He alleges that his family was staying at Commissioner street in Boksburg. Also he alleges that he is from Helbron in the Free State",
		publication: "",
		imgsrc: "/assets/img/b/petrus_fourie.png",
		deceased: null,
		discharged: false
	},
	
	
	{
		firstName: "Elias",
		lastName: "Mina",
		doa: "Date Of Admission - 2017/01/26",
		description:
			"Admitted from Lingelethu Home which was operating from the old SANTA hospital building. He is alleged to be coming from 2768 More Street, Daveyton. Social Worker from DSD Benoni went for a home visit but was unable to get the address.",
		publication: "",
		imgsrc: "/assets/img/b/elias_mina.png",
		deceased: null,
		discharged: false
	},
	{
		firstName: "Sinky",
		lastName: "Mahlangu",
		doa: "Date Of Admission - 2004/08/16",
		description:
			"Admitted from Pretoria Hospital. No name, no ID. Name and Surname also given by Social Worker who applied for ID book and grant.",
		publication: "Published twice in Daily Sun Newspaper, with no response.",
		imgsrc: "/assets/img/b/sinky_mahlangu.png",
		deceased: null,
		discharged: false
	}
	,
	{
		firstName: "Andre",
		lastName: "Olivier",
		doa: "Date Of Admission - 2018/03/01",
		description:
			"He was admitted from Bertha Gxowa Hospital in Germiston. He has two brothers, Klaas Olivier staying in Nelspruit and Jappie in George.",
		publication: "",
		imgsrc: "/assets/img/b/andre_olivier.png",
		deceased: null,
		discharged: true
	},
	{
		firstName: "David",
		lastName: "Mthembu",
		doa: "Date Of Admission - 2015/06/28",
		description:
			"Admitted from Charlotte Maxeke Johannesburg Academic Hospital. He alleges that he comes from Mhlabauyalingana in Natal. Social Worker is still trying to get his family in Natal.",
		publication: "",
		imgsrc: "/assets/img/b/david_mthembu.png",
		deceased: null,
		discharged: true
	},
	{
		firstName: "Willem",
		lastName: "Nel",
		doa: "Date Of Admission - 2017/02/04",
		description:
			"He was admitted from Tambo Memorial hospital in Boksburg. He cannot recall the address of the next of kin or family. Difficult to trace his family.",
		publication: "",
		imgsrc: "/assets/img/b/willem_nel.png",
		deceased: null,
		discharged: true
	},
	{
		firstName: "Nelson",
		lastName: "Kgobe",
		doa: "Date Of Admission - 1992/01/11",
		description: "Does not know address of family.",
		publication: "Published twice in Daily Sun Newspaper, with no response.",
		imgsrc: "/assets/img/b/nelson_kgobe.png",
		deceased: null,
		discharged: true
	},
	{
		firstName: "Elijah",
		lastName: "Malinga",
		doa: "Date Of Admission - 2017/01/26",
		description: "Admitted from Lingelethu Home, Daveyton.",
		publication: "Published in Daily Sun Newspaper, with no response.",
		imgsrc: "/assets/img/b/elijah_malinga.png",
		deceased: null,
		discharged: true
	},
		{
		firstName: "Jessie",
		lastName: "van Aswegen",
		doa: "Date Of Admission - 2008/09/05",
		description:
			"Admitted from Shepherds Flock Ministry. She does not remember the correct address of her family except her daughter who is deaf and dumb who is staying at a home for the hearing impaired persons. ",
		publication: "",
		imgsrc: "/assets/img/b/jessie_van_aswegen.png",
		deceased: null,
		discharged: true
	},
	{
		firstName: "Gladys",
		lastName: "Mkhare",
		doa: "Date Of Admission - 2012/01/11",
		description:
			"Was admitted from Struitbult in Nigel. She is deaf and it is difficult to get information about her family. She had alleged that she comes from Atteridgeville later changed that she was from Soshanguve.",
		publication:
			"Published twice in Pretoria Newspaper and Daily Sun Newspaper, with no response.",
		imgsrc: "/assets/img/b/gladys_mkhare.png",
		deceased: null,
		discharged: true
	},
]
export default beneficiaries
