const founders = [
	{
		firstName: "Sithole Joseph",
		lastName: "Letlala",
		position: "Executive Director",
		description: `Mr. Letlala is a trained Health Care Professional in Free State Province in Welkom at Ernest Oppenheimer Hospital. <br /> Was instrumental in the establishment of the Christian Services Foundation Nursing School.`,
		imgsrc: "/assets/img/t/p/sithole_joseph_letlala.jpg",
		social: [
			{
				tw: "",
				lkdn: "",
				readme: "",
			},
		],
	},
	{
		firstName: "Sylvia",
		lastName: "Modiselle",
		position: "Nursing Service Manager",
		description: `Trained and qualified as a General Nurse, Midwife and Operating Theatre Nursing Science at Natalspruit Hospital. <br /> The first trainee of Natalspruit Hospital to be appointed as a Chief Professional Nurse.`,
		imgsrc: "/assets/img/t/p/sylvia_modiselle.jpg",
		social: [
			{
				tw: "",
				lkdn: "",
				readme: "",
			},
		],
	},
	{
		firstName: "Fairhope",
		lastName: "Manuel",
		position: "Social Worker",
		description: `She has worked for the Gauteng Provincial Government for 14 years as a Social Worker and left in 1984 being promoted as a Senior Social Worker. <br /> She has also worked for Rhema Service Foundation as a Social Worker for Emseni Chronic Care Centre.`,
		imgsrc: "/assets/img/t/p/fairhope_manuel.jpg",
		social: [
			{
				tw: "",
				lkdn: "",
				readme: "",
			},
		],
	},
]

const advisoryBoard = [
	{
		firstName: "Jabulani",
		lastName: "Dubula",
		position: "Chairperson",
		description: `18 years of experience in the public service area of expertise grant funding, procurement and executive support.`,
		imgsrc: "/assets/img/t/p/jabulani_dubula.jpg",
		social: [
			{
				tw: "",
				lkdn: "https://za.linkedin.com/in/jabulani-dubula-936ab9139",
				readme: "",
			},
		],
	},

	{
		firstName: "Mzwa",
		lastName: "Ndimande",
		position: "Treasurer",
		description: `I am  the Internal Auditor for a manufacturing company.  I have worked in finance space for more than ten years. Joined TFCC in 2018 in pursuit of helping those in need.`,
		imgsrc: "/assets/img/t/p/mzwa_ndimande.jpg",
		social: [
			{
				tw: "",
				lkdn: "",
				readme: "",
			},
		],
	},
	{
		firstName: "Milton Tsekiso",
		lastName: "Marite",
		position: "Deputy Chairperson",
		description: `Human Resources, Financial Management and Business Administration. Ex-Councillor for Wedela transitional council.`,
		imgsrc: "/assets/img/t/p/milton_tsekiso_marite.jpg",
		social: [
			{
				tw: "",
				lkdn: "",
				readme: "",
			},
		],
	},
	// {
	// 	firstName: "Letlhogonolo",
	// 	lastName: "Gaborone",
	// 	position: "Board Member",
	// 	description: `An attorney and currently employed by the National Union of Mine Workers.`,
	// 	imgsrc: "/assets/img/t/p/letlhogonolo_gaborone.jpg",
	// 	social: [
	// 		{
	// 			tw: "",
	// 			lkdn: "https://za.linkedin.com/in/letlhogonolo-gaborone-8720a941",
	// 			readme: "",
	// 		},
	// 	],
	// },
	{
		firstName: "Gomolemo",
		lastName: "Phantshang",
		position: "Board Member",
		description: `Legal and Compliance Advisor with 8years experience in the Financial Industry (including 4 years' experience with a Medical Scheme administrator and managed healthcare organisation).`,
		imgsrc: "/assets/img/t/p/gomolemo_phantshang.jpg",
		social: [
			{
				tw: "",
				lkdn: "https://za.linkedin.com/in/gomolemo-phantshang-61a61b2a",
				readme: "",
			},
		],
	},
	{
		firstName: "Itumeleng",
		lastName: "Molatlhegi",
		position: "Board Member",
		description: `Founder of Exclusive Executive Consulting Consortium. 10 years Strategic Planning Management experience with an MBA.`,
		imgsrc: "/assets/img/t/p/itumeleng_molatlhegi.jpg",
		social: [
			{
				tw: "",
				lkdn: "https://za.linkedin.com/in/itumeleng-molatlhegi-a327aa2b",
				readme: "",
			},
		],
	},
	{
		firstName: "Kedibone",
		lastName: "Mdolo",
		position: "Board Member",
		description: `Kedibone is an active member of the International Council of Nurses (ICN) and the Democratic Nurses Organisation of South Africa (DENOSA)`,
		imgsrc: "/assets/img/t/p/kedibone_mdolo.jpg",
		social: [
			{
				tw: "",
				lkdn: "https://za.linkedin.com › kedibone-mdolo-3b210124",
				readme: "",
			},
		],
	},
	{
		firstName: "Dr Bobby",
		lastName: "Ramasia",
		position: "Board Member",
		description: `A medical doctor by profession, Bobby has a wealth of clinical and management expertise derived from a 25-year career in the health-care industry. Serves on the boards of the Board of Healthcare Funders and Health Quality Assessment.`,
		imgsrc: "/assets/img/t/p/bobby_ramasia.jpg",
		social: [
			{
				tw: "",
				lkdn: "https://za.linkedin.com/in/bobby-ramasia-a5600328",
				readme:
					"http://discoverydrillinggroup.co.za/dr-bobby-ramasia-director/",
			},
		],
	},
	{
		firstName: "Amy",
		lastName: "Phoshoko",
		position: "Board Member",
		description: `Amy has accumulated a wealth of experience in the banking industry of the financial services absa group. She holds a bachelor’s degree in commerce and has been with the bank for 15 years. `,
		imgsrc: "/assets/img/t/p/amy_phoshoko.png",
		social: [
			{
				tw: "",
				lkdn: "",
				readme: "",
			},
		],
	},
	{
		firstName: "Matlhodi",
		lastName: "Kubeka",
		position: "Board Member",
		description: `Enim donec ultrices enim euismod mi praesent rutrum aliquam. Varius lobortis sociis inceptos molestie. Bibendum imperdiet lacinia consectetuer nec sagittis. `,
		imgsrc: "/assets/img/t/p/matlhodi_kubeka.jpg",
		social: [
			{
				tw: "",
				// lkdn: "https://za.linkedin.com/in/matlhodi-sybil-kubeka-86607a85",
				readme: "",
			},
		],
	},
	{
		firstName: "Moses",
		lastName: "Shandukane",
		position: "Board Member",
		description: `Enim donec ultrices enim euismod mi praesent rutrum aliquam. Varius lobortis sociis inceptos molestie. Bibendum imperdiet lacinia consectetuer nec sagittis. `,
		imgsrc: "/assets/img/t/p/moses_shandukane.jpg",
		social: [
			{
				tw: "",
				lkdn: "",
				readme: "",
			},
		],
	},
]

const staff = [
	{
		firstName: "Rosie",
		lastName: "Masedi",
		position: "Receptionist",
		description: ``,
		imgsrc: "/assets/img/t/p/rosie_masedi.jpg",
	},
	{
		firstName: "Bibie",
		lastName: "Modisane",
		position: "Ancillary Supervisor",
		description: ``,
		imgsrc: "/assets/img/t/p/bibie_modisane.jpg",
	},
	{
		firstName: "Phuthi",
		lastName: "Motona",
		position: "Pastoral / Counselling",
		description: ``,
		imgsrc: "/assets/img/t/p/phuthi_motona.jpg",
	},
]

// fullName;
// nickname;
// Surname;
// position;
// desc;
// imgSrc;
// social; [
// 	- tw;
// 	- lkdn;
// 	- readme;
// ]
// positionType; [
// 	- Staff
// 	- Founders
// 	- Board Member
// ]

export { founders, advisoryBoard, staff }
