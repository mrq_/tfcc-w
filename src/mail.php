<?php

class Template {
  function get_contents($templateName, $variables) {
    $template = file_get_contents($templateName);

    foreach($variables as $key => $value) {
      $template = str_replace('{{ '.$key.' }}', $value, $template);
    }
    return $template;
  }
}

// Only process POST requests.
if( $_SERVER['REQUEST_METHOD'] !== "POST") {
  header("HTTP/1.0 403 Forbidden");
  echo json_encode([
    "message" => error_get_last(), 
    "status" => http_response_code(403),
    // "message" => "Oops! Something went wrong and we couldn't send your message.", 
  ]);
  exit;
}

$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$subject = filter_input(INPUT_POST, 'subject', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$from = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
$message = filter_input(INPUT_POST, 'content', FILTER_SANITIZE_FULL_SPECIAL_CHARS);

// Check that data was sent to the mailer.
if ( empty($name) || empty($subject) || empty($message) || filter_var($from, FILTER_VALIDATE_EMAIL) === false) {
  // Set a 400 (bad request) response code and exit.
  header("HTTP/1.0 400 Bad Request");
  echo json_encode([
    "message" => error_get_last(), 
    "status" => http_response_code(400),
    // "message" => "Oops! Something went wrong and we couldn't send your message.", 
  ]);
  exit;
}

// Set the recipient email address.
// FIXME: Update this to your desired email address.
// $recipient = "hester95@ethereal.email";
$recipient = "info@tfcc.co.za";

// Set the email subject.
$emailSubject = "TFCC Web Page: " . $subject;

// Build the email content.
// $email_content = "Name: " . $name . "\t E-mail <".$from."> \n\n";
// $email_content .= "Content: " . $subject . "\n\n" . $message;
$email_content = (new Template)->get_contents("assets/contact.tpl", array('name' => $name, 'email' => $from, 'subject' => $subject, 'message' => $message));

// To send HTML mail, the Content-type header must be set
$headers[] = 'MIME-Version: 1.0';
$headers[] = 'Content-type: text/html; charset=UTF-8';
$headers[] = 'X-Priority: 1';
$headers[] = 'X-Mailer: PHP/' . phpversion();

// Additional headers
$headers[] = 'From: '. $name .' <'. $from .'>';
$headers[] = 'Cc: ';
$headers[] = 'Bcc: j.letlala@tfcc.co.za';

// Send the email.
if (mail($recipient, $emailSubject, $email_content, implode("\r\n", $headers))) {
  // Set a 200 (okay) response code.
  header('HTTP/1.0 200 OK');
  echo json_encode([
    "message" => "Thank You! Your message has been sent.", 
    "status" => http_response_code(200)
  ]);
} else {
  // Set a 500 (internal server error) response code.
  header('HTTP/1.0 500 Internal Server Error');
  echo json_encode([
    "message" => error_get_last(), 
    "status" => http_response_code(500),
    // "message" => "Oops! Something went wrong and we couldn't send your message.", 
  ]);
}