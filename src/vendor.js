// console.log("Vendor Bundle.")

/*********************************************
 *****  Import Scripts and Styling
 **/
import "./vendor.scss"
import $ from "jquery"
// import "bootstrap"
import "bootstrap/js/dist/util"
import "bootstrap/js/dist/alert"
// import "bootstrap/js/dist/button"
import "bootstrap/js/dist/carousel"
import "bootstrap/js/dist/collapse"
// import 'bootstrap/js/dist/dropdown'
window.jQuery = $
window.$ = $

/********************************************* */

/******************************************************
 * Document Ready
 ***/
$(function () {
	let current = window.location.href // 'href' property of the DOM element is the absolute path
	$(".navbar a").each(function () {
		// if the current path is like this link, make it active
		if (this.href === current) {
			$(this).parent().addClass("active") // Add class to parent of anchor tag
		}
	})

	$("#toggle").click(function () {
		$(this).toggleClass("active")
		$("#overlay").toggleClass("open")
		$("body").toggleClass("noscroll")
	})

	// ---------------------------------------------
	// NavBar | Transparency Change onScroll
	// ---------------------------------------------
	const navBar = $(".navbar")
	$(window).on("load scroll resize", checkScroll)

	function checkScroll() {
		var startY = navBar.height() * 2 //The point where the navbar changes in px

		if ($(window).scrollTop() > startY) {
			navBar.addClass("navbar-scrolled")
		} else {
			navBar.removeClass("navbar-scrolled")
		}
	}

	// ===== Scroll to Top ====
	$(window).scroll(function () {
		if ($(this).scrollTop() >= 190) {
			// If page is scrolled more than 50px
			$("#return-to-top").fadeIn(200) // Fade in the arrow
		} else {
			$("#return-to-top").fadeOut(200) // Else fade out the arrow
		}
	})

	$("#return-to-top").click(function () {
		// When arrow is clicked
		$("body,html").animate(
			{
				scrollTop: 0, // Scroll to top of body
			},
			500
		)
	})
})
