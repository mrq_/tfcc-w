const path = require("path")
const Dotenv = require("dotenv-webpack")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const HtmlWebpackPartialsPlugin = require("html-webpack-partials-plugin")
const CopyPlugin = require("copy-webpack-plugin")

module.exports = {
	entry: {
		vendor: "./src/vendor.js",
		index: "./src/assets/js/index.js",
		services: "./src/assets/js/service.js",
		beneficiaries: "./src/assets/js/beneficiaries.js",
		gallery: "./src/assets/js/gallery.js",
		about: "./src/assets/js/about.js",
		contact: "./src/assets/js/contact.js"
	},
	devtool: "source-map",
	plugins: [
		new CopyPlugin([
			// { from: "src/assets/mailtest.php", to: "dist/assets/mailtest.php" }
		]),
		new HtmlWebpackPlugin({
			title: "Tswelopele Frail Care Centre",
			minify: {
				collapseWhitespace: false,
				removeComments: true
			},
			hash: true,
			filename: "index.html",
			favicon: "./src/assets/img/favicon.png",
			excludeChunks: [
				"services",
				"beneficiaries",
				"gallery",
				"about",
				"contact"
			],
			template: "./src/views/pages/index.ejs",
			meta: {
				description:
					"Tswelopele Frail Care Centre is a non-profit company providing affordable, secure and caring accommodation and services to the elderly.",
				keywords:
					"health,frail care,frail care centre,frail,frail centre,non-profit,secure accommodation,caring accommodation,secure and caring accommodation,services to the elderly,tswelopele,Tswelopele Frail Care Centre",
				author: "mrq letlala"
			},
			cache: true,
			showErrors: true
		}),
		new HtmlWebpackPlugin({
			title: "Service | Tswelopele Frail Care Centre",
			minify: {
				collapseWhitespace: false,
				removeComments: true
			},
			hash: true,
			filename: "services.html",
			favicon: "./src/assets/img/favicon.png",
			excludeChunks: ["index", "beneficiaries", "gallery", "about", "contact"],
			template: "./src/views/pages/service.ejs",
			meta: {
				description:
					"Tswelopele Frail Care Centre is a non-profit company providing affordable, secure and caring accommodation and services to the elderly.",
				keywords:
					"health,frail care,frail care centre,frail,frail centre,non-profit,secure accommodation,caring accommodation,secure and caring accommodation,services to the elderly,tswelopele,Tswelopele Frail Care Centre",
				author: "mrq letlala"
			}
		}),
		new HtmlWebpackPlugin({
			title: "Beneficiaries | Tswelopele Frail Care Centre",
			minify: {
				collapseWhitespace: false,
				removeComments: true
			},
			hash: true,
			filename: "beneficiaries.html",
			favicon: "./src/assets/img/favicon.png",
			excludeChunks: ["index", "services", "gallery", "about", "contact"],
			template: "./src/views/pages/beneficiaries.ejs",
			meta: {
				description:
					"Tswelopele Frail Care Centre is a non-profit company providing affordable, secure and caring accommodation and services to the elderly.",
				keywords:
					"health,frail care,frail care centre,frail,frail centre,non-profit,secure accommodation,caring accommodation,secure and caring accommodation,services to the elderly,tswelopele,Tswelopele Frail Care Centre",
				author: "mrq letlala"
			}
		}),
		new HtmlWebpackPlugin({
			title: "Gallery | Tswelopele Frail Care Centre",
			minify: {
				collapseWhitespace: false,
				removeComments: true
			},
			hash: true,
			filename: "gallery.html",
			favicon: "./src/assets/img/favicon.png",
			excludeChunks: ["index", "services", "beneficiaries", "about", "contact"],
			template: "./src/views/pages/gallery.ejs",
			meta: {
				description:
					"Tswelopele Frail Care Centre is a non-profit company providing affordable, secure and caring accommodation and services to the elderly.",
				keywords:
					"health,frail care,frail care centre,frail,frail centre,non-profit,secure accommodation,caring accommodation,secure and caring accommodation,services to the elderly,tswelopele,Tswelopele Frail Care Centre",
				author: "mrq letlala"
			}
		}),
		new HtmlWebpackPlugin({
			title: "About Us | Tswelopele Frail Care Centre",
			minify: {
				collapseWhitespace: false,
				removeComments: true
			},
			hash: true,
			filename: "about.html",
			favicon: "./src/assets/img/favicon.png",
			excludeChunks: [
				"index",
				"services",
				"beneficiaries",
				"gallery",
				"contact"
			],
			template: "./src/views/pages/about.ejs",
			meta: {
				description:
					"Tswelopele Frail Care Centre is a non-profit company providing affordable, secure and caring accommodation and services to the elderly.",
				keywords:
					"health,frail care,frail care centre,frail,frail centre,non-profit,secure accommodation,caring accommodation,secure and caring accommodation,services to the elderly,tswelopele,Tswelopele Frail Care Centre",
				author: "mrq letlala"
			}
		}),
		new HtmlWebpackPlugin({
			title: "Contact Us | Tswelopele Frail Care Centre",
			minify: {
				collapseWhitespace: false,
				removeComments: true
			},
			hash: true,
			filename: "contact.html",
			favicon: "./src/assets/img/favicon.png",
			excludeChunks: ["index", "services", "beneficiaries", "gallery", "about"],
			template: "./src/views/pages/contact.ejs",
			meta: {
				description:
					"Tswelopele Frail Care Centre is a non-profit company providing affordable, secure and caring accommodation and services to the elderly.",
				keywords:
					"health,frail care,frail care centre,frail,frail centre,non-profit,secure accommodation,caring accommodation,secure and caring accommodation,services to the elderly,tswelopele,Tswelopele Frail Care Centre",
				author: "mrq letlala"
			}
		}),
		new HtmlWebpackPartialsPlugin([
			{
				path: "./src/views/partials/nav.html",
				priority: "high",
				template_filename: "*"
			},
			{
				path: "./src/views/partials/footer.html",
				priority: "low",
				template_filename: "*"
			}
		]),
		new Dotenv()
	],
	module: {
		rules: [
			{
				test: /\.html$/,
				loader: "html-loader",
				options: {
					interpolate: true
				}
			},
			{
				test: /\.php$/,
				loader: "file-loader",
				options: {
					name: "[name].[ext]"
					// context: "src/"
					// outputPath: "assets/" // where the fonts will go
					// publicPath: path.resolve("assets", "img") // override the default path
				}
			},
			{
				test: /\.tpl$/,
				loader: "file-loader",
				options: {
					name: "assets/[name].[ext]"
					// context: "src/"
					// outputPath: "assets/" // where the fonts will go
					// publicPath: path.resolve("assets", "img") // override the default path
				}
			}
		]
	}
}
