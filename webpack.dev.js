const path = require("path")
const common = require("./webpack.common")
const merge = require("webpack-merge")
const HtmlWebpackPlugin = require("html-webpack-plugin")

module.exports = merge(common, {
	mode: "development",
	output: {
		filename: "[name].bundlr.js",
		path: path.resolve(__dirname, "dist")
	},
	devtool: "source-map",
	plugins: [],
	module: {
		rules: [
			{
				test: /\.scss$/,
				use: [
					{
						loader: "style-loader", // 3. Injects styles into DOM
						options: {
							sourceMap: true,
							convertToAbsoluteUrls: true
						}
					},
					{
						loader: "css-loader", // 2. Turns css into commonjs
						options: {
							url: true,
							sourceMap: true
						}
					},
					{
						loader: "sass-loader", // 1. Turns SASS into CSS
						options: {
							sourceMap: true
						}
					}
				]
			},
			{
				test: /\.(png|jp?g|gif|pdf)$/,
				use: {
					loader: "file-loader",
					options: {
						name: "[path][name].[ext]",
						context: path.resolve(__dirname, "src/")
						// outputPath: "assets/img/", // where the fonts will go
						// publicPath: path.resolve("assets", "img") // override the default path
					}
				}
			},
			{
				test: /\.(eot|woff|woff2|ttf|svg)(\?\S*)?$/,
				loader: "file-loader",
				options: {
					limit: 10000,
					name: "[name].[ext]",
					outputPath: "assets/fonts" // where the fonts will go
				}
			},
			{
				type: "javascript/auto",
				test: /\.json$/,
				use: [
					{
						loader: "file-loader",
						options: {
							name: "[name].[ext]",
							outputPath: "assets/js/data" // where the fonts will go
						}
					}
				]
			}
		]
	}
})
