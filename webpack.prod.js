const path = require("path")
const common = require("./webpack.common")
const merge = require("webpack-merge")
const CleanWebpackPlugin = require("clean-webpack-plugin")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin")
const TerserPlugin = require("terser-webpack-plugin")

module.exports = merge(common, {
	mode: "production",
	output: {
		filename: "assets/js/[name].[contentHash].bundlr.js",
		path: path.resolve(__dirname, "dist"),
	},
	optimization: {
		minimizer: [new OptimizeCssAssetsPlugin(), new TerserPlugin()],
		minimize: true,
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: "assets/css/[name].[contentHash].css",
		}),
		new CleanWebpackPlugin(),
	],
	module: {
		rules: [
			{
				test: /\.(sc|c)ss$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader, // 3. Extract CSS into files
						options: {
							// you can specify a publicPath here
							// by default it uses publicPath in webpackOptions.output
							publicPath: "../../",

							// convertToAbsoluteUrls: true
							// publicPath: (resourcePath, context) => {
							// 	// publicPath is the relative path of the resource to the context
							// 	// e.g. for ./css/admin/main.css the publicPath will be ../../
							// 	// while for ./css/main.css the publicPath will be ../
							// 	return path.relative(path.dirname(resourcePath), context) + "/"
							// }
						},
					},

					{
						loader: "css-loader", // 2. Turns css into commonjs
						options: {
							// url: true
							// sourceMap: true
						},
					},
					{
						loader: "sass-loader", // 1. Turns SASS into CSS
						options: {
							// sourceMap: true
						},
					},
				],
			},
			{
				test: /\.(pdf)$/,
				use: [
					{
						loader: "file-loader",
						options: {
							name: "[path][name].[ext]",
							context: "src/",
							// context: path.resolve(__dirname, "src/assets/img/"),
							// outputPath: "assets", // where the fonts will go
							publicPath: "../", // override the default path
						},
					},
				],
			},
			{
				test: /\.(png|jp?g|gif)$/,
				use: [
					{
						loader: "file-loader",
						options: {
							// Images larger than 10 KB won’t be inlined
							limit: 10 * 1024,
							name: "[path][name].[ext]",
							// context: "src/assets/img/",
							context: path.resolve(__dirname, "src/assets/img/"),
							outputPath: "assets/img/", // where the fonts will go
							// publicPath: "../img" // override the default path
						},
					},
					{
						loader: "image-webpack-loader",
					},
				],
			},
			{
				test: /\.(eot|woff|woff2|ttf|svg)(\?\S*)?$/,
				loader: "file-loader",
				options: {
					limit: 10000,
					name: "[name].[ext]",
					outputPath: "assets/fonts", // where the fonts will go
					publicPath: "../fonts", // override the default path
				},
				exclude: [/\\assets\\fonts\\.*\.svg([\?\#].*)?$/],
			},
			{
				type: "javascript/auto",
				test: /\.json$/,
				use: [
					{
						loader: "file-loader",
						// include: [path.resolve(__dirname, "src")],
						options: {
							limit: 10000,
							name: "[name].[ext]",
							outputPath: "assets/js/data", // where the fonts will go
							// publicPath: "../js/data" // override the default path
						},
					},
				],
			},
		],
	},
})
